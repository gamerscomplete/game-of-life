package main

import (
	"flag"
	"fmt"
	"github.com/g3n/engine/core"
	"github.com/g3n/engine/geometry"
	"github.com/g3n/engine/graphic"
	"github.com/g3n/engine/light"
	"github.com/g3n/engine/material"
	"github.com/g3n/engine/math32"
	//	"github.com/g3n/engine/util/application"
	"github.com/g3n/engine/util/logger"
	"github.com/g3n/engine/window"
	"gitlab.com/gamerscomplete/game-of-life/application"
	"time"
)

const (
	APP_NAME           = "Game of life"
	SCREEN_WIDTH       = 800
	SCREEN_HEIGHT      = 600
	CUBE_HEIGHT        = float32(1)
	CUBE_WIDTH         = float32(1)
	CUBE_LENGTH        = float32(1)
	BOARD_BLOCK_SPACER = float32(0.25)
)

var (
	_cubeOffColor = math32.Color{0.7, 0.7, 0}
	_cubeOnColor  = math32.Color{0, 0, 1}
	_boardOffset  = math32.Vector3{0, 0, 0}

//	_lookAt       = math32.Vector3{0, 0, 0}
)

type Cube struct {
	*graphic.Mesh
	active bool
}

type Board struct {
	width          int
	height         int
	blocks         [][]*Cube
	app            *application.Application
	node           *core.Node
	managerRunning bool
	quit           chan struct{}
	running        bool
	ticker         *time.Ticker
	simSpeed       time.Duration
	iterations     int
}

type Raycast struct {
	rc *core.Raycaster
}

func main() {
	var width = flag.Int("width", 134, "Board width")
	var height = flag.Int("height", 100, "Board height")
	var simSpeed = flag.Int("speed", 500, "Simulation speed. Speed in milliseconds")

	app, err := application.Create(application.Options{
		Title:       APP_NAME,
		Width:       SCREEN_WIDTH,
		Height:      SCREEN_HEIGHT,
		Fullscreen:  false,
		LogPrefix:   "DG",
		LogLevel:    logger.DEBUG,
		TargetFPS:   60,
		EnableFlags: true,
	})
	if err != nil {
		panic(err)
	}

	addLighting(app)

	widthCenter := (((BOARD_BLOCK_SPACER * float32(*width)) - 1) + float32(*width)) / 2
	heightCenter := (((BOARD_BLOCK_SPACER * float32(*height)) - 1) + float32(*height)) / 2
	lookAt := math32.Vector3{X: widthCenter, Y: 0, Z: heightCenter}
	cameraPos := math32.Vector3{X: widthCenter, Y: 100, Z: heightCenter}
	addCamera(app, lookAt, cameraPos)

	board := NewBoard(app, *width, *height)
	board.drawBoard()

	var caster Raycast
	caster.rc = core.NewRaycaster(&math32.Vector3{}, &math32.Vector3{})
	caster.rc.LinePrecision = 0.05
	caster.rc.PointPrecision = 0.05

	// Subscribe to mouse button down events to flip blocks
	app.Window().Subscribe(window.OnMouseDown, func(evname string, ev interface{}) {
		caster.onMouse(app, ev, board)
	})

	app.Window().Subscribe(window.OnKeyDown, board.onKey)

	board.StartManager(*simSpeed)

	fmt.Println("X:", *width, "Y:", *height, "Total blocks:", *width**height)
	fmt.Println("Simulation speed:", *simSpeed, "ms")

	if err := app.Run(); err != nil {
		fmt.Println("Run failed:", err)
		return
	}
}

func (board *Board) onKey(evname string, ev interface{}) {
	key, found := ev.(*window.KeyEvent)
	if !found {
		fmt.Println("Failed to assert keyevent")
		return
	}
	switch key.Keycode {
	case window.KeyP:
		//Pause
		fmt.Println("Pausing simulation")
		board.running = false
	case window.KeyS:
		//start
		fmt.Println("Starting simulation")
		board.running = true
	case window.KeyUp:
		board.SetSimSpeed(board.GetSimSpeed() + (50 * time.Millisecond))
	case window.KeyDown:
		board.SetSimSpeed(board.GetSimSpeed() - (50 * time.Millisecond))
	}
}

func (cube *Cube) IsActive() bool {
	return cube.active
}

func (cube *Cube) SetActive() {
	if cube.active {
		return
	}
	cube.active = true
	cube.SetColor(_cubeOnColor)
}

func (cube *Cube) SetInactive() {
	cube.active = false
	cube.SetColor(_cubeOffColor)
}

func (cube *Cube) SetColor(color math32.Color) {
	gr := cube.GetGraphic()
	imat := gr.GetMaterial(0)

	type matI interface {
		AmbientColor() math32.Color
		SetColor(*math32.Color)
	}

	if v, ok := imat.(matI); ok {
		v.SetColor(&color)
	}
}

func (cube *Cube) ToggleInactive() {
	if cube.IsActive() {
		cube.SetInactive()
	} else {
		cube.SetActive()
	}
}

func (board *Board) SetSimSpeed(simSpeed time.Duration) {
	/*	if (board.simSpeed - simSpeed) < 0 {
			fmt.Println("Duration would fall below 0. Not setting duration")
			return
		}
	*/
	if simSpeed < 1 {
		fmt.Println("Duration less than 1. Skipping set")
		return
	}
	board.simSpeed = simSpeed
	fmt.Println("Setting simspeed to:", board.simSpeed)
	board.ticker = time.NewTicker(board.simSpeed)
}

func (board *Board) GetSimSpeed() time.Duration {
	return board.simSpeed
}

func (board *Board) StartManager(simSpeed int) {
	if board.managerRunning {
		return
	}
	board.managerRunning = true
	board.SetSimSpeed(time.Duration(simSpeed) * time.Millisecond)

	go func() {
		for {
			select {
			case <-board.ticker.C:
				if !board.running {
					continue
				}
				type QueueItem struct {
					setActive bool
					cube      *Cube
				}
				var setQueue []QueueItem
				for key1, value1 := range board.blocks {
					for key2, value2 := range value1 {
						neighbors := board.getNeighborCount(key1, key2)
						if value2.IsActive() {
							// 2. Any live cell with two or three live neighbours lives on to the next generation.
							if neighbors < 2 || neighbors > 3 {
								//								value2.SetInactive()
								setQueue = append(setQueue, QueueItem{setActive: false, cube: value2})
							}
						} else {
							// 4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
							if neighbors == 3 {
								setQueue = append(setQueue, QueueItem{setActive: true, cube: value2})
								//								value2.SetActive()
							}
						}
					}
				}
				for _, value := range setQueue {
					if value.setActive {
						value.cube.SetActive()
					} else {
						value.cube.SetInactive()
					}
				}
				setQueue = setQueue[:0]
				board.iterations++
			case <-board.quit:
				board.ticker.Stop()
				break
			}
		}
		board.managerRunning = false
	}()
}

func (board *Board) getNeighborCount(x, y int) (count int) {
	// We are mc
	//   ------- ------- -------
	//   | 0,0 | | 0,1 | | 0,2 |
	//   ------- ------- -------
	//   ------- ------- -------
	//   | 1,0 | | 1,1 | | 1,2 |
	//   ------- ------- -------
	// ^ ------- ------- -------
	// : | 2,0 | | 2,1 | | 2,2 |
	// Y ------- ------- -------
	//   X -->

	//Upper row
	//0,0
	if board.blockActive(x-1, y-1) {
		count++
	}
	//0,1
	if board.blockActive(x-1, y) {
		count++
	}
	//0,2
	if board.blockActive(x-1, y+1) {
		count++
	}

	//Middle row
	//1,0
	if board.blockActive(x, y-1) {
		count++
	}
	//1,1 is the example input so exclude ourself
	//1,2
	if board.blockActive(x, y+1) {
		count++
	}

	//Bottom row
	//2,0
	if board.blockActive(x+1, y-1) {
		count++
	}
	//2,1
	if board.blockActive(x+1, y) {
		count++
	}
	//2,2
	if board.blockActive(x+1, y+1) {
		count++
	}
	return
}

func (board *Board) blockActive(x, y int) bool {
	//board.blocks[x][y]
	//Make sure we're not greater then the board dimensions
	if x > (len(board.blocks)-1) || y > (len(board.blocks[0])-1) {
		return false
	}

	//Make sure we're not off the other side of the board either
	if x < 1 || y < 1 {
		return false
	}

	if board.blocks[x][y].IsActive() {
		return true
	}
	return false
}

func NewBoard(app *application.Application, width int, height int) *Board {
	node := core.NewNode()
	board := Board{app: app, node: node, width: width, height: height}
	app.Scene().Add(node)
	board.blocks = make([][]*Cube, width)
	for key, _ := range board.blocks {
		board.blocks[key] = make([]*Cube, height)
	}
	board.quit = make(chan struct{})
	return &board
}

func (caster *Raycast) onMouse(app *application.Application, ev interface{}, board *Board) {
	mev := ev.(*window.MouseEvent)
	width, height := app.Window().Size()
	x := 2*(mev.Xpos/float32(width)) - 1
	y := -2*(mev.Ypos/float32(height)) + 1
	app.Camera().SetRaycaster(caster.rc, x, y)

	var intersects []core.Intersect

	for key1, value1 := range board.blocks {
		//		for key2, value2 := range value1 {
		for key2, _ := range value1 {
			intersects = caster.rc.IntersectObject(board.blocks[key1][key2], true)
			if len(intersects) > 0 {
				board.blocks[key1][key2].SetActive()
			}
		}
	}
}

func (board *Board) drawBoard() {
	for x := 0; x < board.width; x++ {
		for z := 0; z < board.height; z++ {
			board.placeBlock(x, z)
		}
	}
}

func NewCube() *Cube {
	geom := geometry.NewSegmentedBox(CUBE_HEIGHT, CUBE_WIDTH, CUBE_LENGTH, 4, 4, 4)
	mat := material.NewStandard(&_cubeOffColor)
	//    mat.SetOpacity(opacity)
	box := graphic.NewMesh(geom, mat)

	cube := new(Cube)
	cube.Mesh = box
	return cube
}

func (board *Board) placeBlock(x int, z int) {
	var pos math32.Vector3
	pos.Set(CUBE_HEIGHT, CUBE_WIDTH, CUBE_LENGTH).Multiply(&math32.Vector3{float32(x) + (float32(x) * BOARD_BLOCK_SPACER), float32(0), float32(z) + (float32(z) * BOARD_BLOCK_SPACER)}).Add(&_boardOffset)
	cube := NewCube()

	cube.SetPosition(pos.X, pos.Y, pos.Z)
	//	board.AddBlock(cube)

	//	fmt.Println("Placing block at x:", x, "Z:", z)
	board.blocks[x][z] = cube
	board.node.Add(cube)

	//	board.blocks = append(board.blocks, cube)
	//	fmt.Println("Placing cube count:", len(board.blocks))
	//	board.app.Scene().Add(cube)
}

func addLighting(app *application.Application) {
	//////////
	// Lighting
	l1 := light.NewDirectional(&math32.Color{1, 1, 1}, 1.0)
	l1.SetPosition(0, 50, 10)
	app.Scene().Add(l1)

	app.Gl().ClearColor(1, 0, 0, 1.0)

	// Adds ambient light to the test scene
	ambLight := light.NewAmbient(&math32.Color{1.0, 1.0, 1.0}, 0.5)
	app.Scene().Add(ambLight)
}

func addCamera(app *application.Application, lookat math32.Vector3, camPos math32.Vector3) {
	// Sets perspective camera position
	width, height := app.Window().Size()

	aspect := float32(width) / float32(height)
	app.CameraPersp().SetPosition(camPos.X, camPos.Y, camPos.Z)
	app.CameraPersp().LookAt(&lookat)
	app.CameraPersp().SetAspect(aspect)

	// Default camera is perspective
	app.SetCamera(app.CameraPersp())
	// Adds camera to scene (important for audio demos)
	app.Scene().Add(app.Camera().GetCamera())
}
